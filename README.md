# COVID-19 Modelling developed by Hertfordshire & West Essex STP

This model combines a model predicting the number of cases (doubling, curve or SEIR) with a patient flow model. It is important to recognise the caveats of this model and to understand the assumptions you make when running it. Our assumptions are NOT included in this code so running the model will without altering  assumptions.csv will produce meaningless results.

*  This work is in active development and substantial, breaking changes should be expected frequently.
*  While efforts have been made to ensure the model is valid it has been developed rapidly in a local system and statistical shortcuts and assumptions have been made.
*  Similarly, while efforts have been made to check for coding errors it is possible that errors and bugs may occur. 
*  You are advised to sense check any results using local intelligence.
*  Modelling is to inform risk tolerance around planning - it does not provide hard numbers to plan for.
*  No warranty, expressed or implied, with respect to the code or documentation is made.


This script was created by Public Health Evidence & Intelligence, Hertfordshire County Council in collaboration with East & North Herts, Herts Valleys, and West Essex CCGs, Central London Community Healthcare, East & North Herts, Hertfordshire Community, Princess Alexandra, and West Herts NHS Trusts.

The SEIR model was adapted from work undertaken by Sheffield Council.

Assumptions HWE have used as well as a talk on the development of the model can be found [here](https://future.nhs.uk/DataAnalyticsCovid19/view?objectID=20001648) (account required)

We are very open to wider collaboration and feedback - in the first instance please open an issue with a collaboration label.


# Adapting for local use

## Excel

1. Alter the populations in Case Weightings sheet
2. Enter the assumptions in the Assumptions sheet (you are strongly advised to validate locally)
3. Enter the details of a curve or SEIR model as needed 
    - Curves models work by assuming that the number of cases found until the start date represents x% of the curve
    - SEIR models use the Rts table, the cases table and optionally hte Imperial estimated Rts table, all found onthe Model sheet
4. Select Doubling, Curve or SEIR from Model sheet cell C4

Many of these functions can be linked to external sources of data or otherwise streamlined through pivottables, powerquery/pivot etc. These are not provided in this release.

## R

1. Update the contents of the data folder to make it locally relevant (you are strongly advised to validate assumptions locally)
2. The covid_model.R file contains model set up parameters that will need to be changed.
3. Sections labelled "1 Run" will just run the model once while those labelled "Looped" will rerun for a range of areas and Rts
